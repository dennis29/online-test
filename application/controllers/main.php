<?php
if (!defined('BASEPATH'))
exit ('No Direct script allowed');
class Main extends CI_Controller
{
	function __construct()
	{
	parent ::__construct();
	$this->func->koneksi_database();
	}
	function index()
	{
	$data=array();
	$data=$this->func->konek();
	if(!isset($_GET['page'])){
	$data['page']="ujian";
	}
	else{
	$data['page']=htmlentities($_GET['page']);}	
	$this->load->view("inclibrary");
	$this->load->view("js/jquery-1.8.3.js");
	$this->load->view("js/script.js");
	$this->load->view("js/unserialize.js");
	$this->load->view("js/utf8.decode.js");
	$this->load->view("js/jslogin.js");
    $this->load->view("js/jsinput.js");
	$this->load->view("css/bootstrap.min.css");
	$this->load->view("css/justified-nav.css");
	$this->load->view("js/css/ui-lightness/jquery-ui-1.8.10.custom.css");
	$this->load->view("js/jquery-ui-1.8.10.custom.min.js");
    $this->load->view("ujian_online/index",$data);	
	if (isset($_POST['idadmin'])){
	$this->session->set_userdata("id_admin",$_POST['idadmin']);
	$this->session->set_userdata("user_admin",$_POST['passwordadmin']);
	$data['id_admin']=$this->session->userdata("id_admin");
	$data['user_admin']=$this->session->userdata("user_admin");
		}
	elseif (isset($_POST['nip'])){
	$this->session->set_userdata("id_guru",$_POST['nip']);
	$this->session->set_userdata("user_guru",$_POST['passwordguru']);
	$data['id_guru']=$this->session->userdata("id_guru");
	$data['user_guru']=$this->session->userdata("user_guru");
	}
	elseif (isset($_POST['nis'])){
	$this->session->set_userdata("id_siswa",$_POST['nis']);
	$this->session->set_userdata("user_siswa",$_POST['passwordsiswa']);
	$data['id_siswa']=$this->session->userdata("id_siswa");
	$data['user_siswa']=$this->session->userdata("user_siswa");
	$this->session->set_userdata("soal",$this->func->buatsessionsoalrandom($data['koneksi'],$data['db']));
	$data['soal']=array("soal"=>$this->session->userdata("soal"));
	}
	elseif((isset($_POST['submitjawab']))||(isset($_POST['submithiddenjawab'])) ||(isset($_POST['submitsimpanjawaban'])) ){
	$this->session->set_userdata("id_siswa",$_POST['id_siswa']);
	$data['id_siswa']=$this->session->userdata("id_siswa");
	}
	if (isset($_GET['sessionDestroy'])){
	$this->session->sess_destroy();
	}
	$this->load->view("ujian_online/body",$data);	
	}	
	function cetaklaporansiswa()
	{
	$data=$this->func->konek();
	$pdfphp=$this->load->view("fpdf17/fpdf");
	$this->func->cetaklaporansiswa($_GET['kelasuntukcetak'],$_GET['jurusanuntukcetak'],$_GET['tahununtukcetak'],$data['koneksi'],$data['db'],$pdfphp);
	}
	function ceklogin()
	{
	$data=array();
	$data=$this->func->konek();
	$this->func->ceklogin($_POST['kode'],$_POST['password'],$_POST['authen'],$data['koneksi'],$data['db']);
   	}
	function ubah_jam_keluar()
	{
	if (isset($_POST['set_jam_keluar'])){
	$jam_keluar=$_POST['set_jam_keluar'];
	$pesan="0";
	if (!empty($jam_keluar)){
	$cekjam_keluar=explode(":",$jam_keluar);
	if (($cekjam_keluar[0] < 24) && ($cekjam_keluar[1] < 60)) 
	{
	$data=array();
	$this->func->ubah_jam_keluar($jam_keluar);
	$data['id_guru']=$this->session->userdata("id_guru");
	$data['user_guru']=$this->session->userdata("user_guru");
	redirect("../#tabs-3",$data);
	}
	}
	echo $pesan;
	}}
	function buatKode()
	{
	$data=array();
	$data=$this->func->konek();
	$this->func->buatKode($_POST['kode'],$_POST['inisial'],$data['koneksi'],$data['db']);
   	}
	function jawaban(){
			if(isset($_POST['id_siswa'])){	
	?>
	<div>
<?php
$id_siswa=$_POST["id_siswa"];
$user_siswa=$_POST["user_siswa"];
	$score=0;
			$benar=0;
			$salah=0;
			$kosong=0;
?>
        <h1>Hasil Jawaban <?php echo ucwords($user_siswa);?></h1>
        
	   <?php 
       if((isset($_POST['submit']))||(isset($_POST['submithidden']))){
			if(isset($_POST['pilihan']))
			{$pilihan=$_POST["pilihan"];}
			else{$pilihan="";}
			$id_soal=$_POST["id"];
			$jumlah=$_POST['jumlah'];			
			
			for ($i=0;$i<$jumlah;$i++){
				//id nomor soal
				$nomor=$id_soal[$i];
				
				//jika user tidak memilih jawaban
				if (empty($pilihan[$nomor])){
					$kosong++;
				}else{
					//jawaban dari user
					$jawaban=$pilihan[$nomor];
					//cocokan jawaban user dengan jawaban di database
					$query=$this->db->where("id_soal",$nomor)->where("jawaban",$jawaban)->get('tabel_soal');		
					$cek=$query->num_rows();
					
					if($cek){
						//jika jawaban cocok (benar)
						$benar++;
					}else{
						//jika salah
						$salah++;
					}
					
				} 
				$score = $benar*10;
			}
		}
		?>
        <form action="./simpan_hasil_ujian" method="post">
		<table width="100%" border="0">
		<tr>
			<td><font color="#black">ID Anda</font></td><td><font color="#black">= <?php echo $id_siswa;?></font></td>
		</tr>
		<tr>
			<td width="12%"><font color="#black">Benar</font></td><td width="88%"><font color="#black">= <?php echo $benar;?> soal x 5 point</font></td>
		</tr>
		<tr>
			<td><font color="#black">Salah</font></td><td><font color="#black">= <?php echo $salah;?> soal </font></td>
		</tr>
		<tr>
			<td><font color="#black">Kosong</font></td><td><font color="#black">= <?php echo $kosong;?> soal </font></td>
		</tr>
		<tr>
			<td><font color="#black"><b>Score</b></font></td><td><font color="#black">= <b><?php echo $score;?></b> Point</font></td>
		</tr>
		</table> 
        <input type="hidden" name="id_siswa" value="<?php echo $id_siswa; ?>" />
        <input type="hidden" name="benar" value="<?php echo $benar;?>" />
        <input type="hidden" name="salah" value="<?php echo $salah;?>" />
        <input type="hidden" name="kosong" value="<?php echo $kosong;?>" />
        <input type="hidden" name="point" value="<?php echo $score;?>" />
        <p></p>
        <input type="submit" name="submit" id="submit" value="Simpan"/>
        
        </form> 
		

</div>
<?php
echo "
<script>
$(document).ready(function() {  

$('#submit').click();
});
</script>
";
}
}
	function simpan_hasil_ujian(){
		if(isset($_POST['id_siswa'])){
		$id_siswa=$_POST['id_siswa'];		
		$benar=$_POST['benar'];
		$salah=$_POST['salah'];
		$kosong=$_POST['kosong'];
		$point=$_POST['point'];
		$tanggal=date("Y-m-d");
		
		$query=mysql_query("insert into tabel_nilai values('','$id_siswa','$benar','$salah','$kosong','$point','$tanggal')");
		
		if($query){
			session_destroy();
			?><script language="javascript">document.location.href='../../'</script><?php
		}else{
			echo mysql_error();
		}
		
	}
	
	
	}
	function carinissiswabaru()
	{
	if (isset($_POST['jurusan'])){
	$data=array();
	$data=$this->func->konek();
	$this->func->carinissiswabaru($_POST['jurusan'],$_POST['kls'],$data['koneksi'],$data['db']);}
   	}
    function simpaninputgurubaru()
	{
	$data=array();
	$data=$this->func->konek();
	$this->func->simpaninputgurubaru($_POST['kode'],$_POST['nama'],$_POST['password'],$data['koneksi'],$data['db']);
   	}
	function simpaninputsiswabaru()
	{
	$data=array();
	$data=$this->func->konek();
	$this->func->simpaninputsiswabaru($_POST['nisuntukinputsiswa'],$_POST['namauntukinputsiswa'],$_POST['kelasuntukinputsiswa'],$_POST['jurusanuntukinputsiswa'],$_POST['passworduntukinputsiswa'],$data['koneksi'],$data['db']);
   	}

}
?>