<script>
function buatKode(kode,inisial,input)
{
$.post( 'index.php/main/buatKode',{kode : kode,inisial:inisial }, function( data){
input.val(data);	
});	
}

         $(document).ready(function(){
		 //Untuk Login Admin
		 $('#salahloginadmin').hide();
	            $('#submitloginadmin').click(function(e){
				salah=false;
                    if($('#idadmin').val()==''){
                        $('#idadmin').addClass('error');
						salah=true;
                    }
                    else{
                        $('#idadmin').removeClass('error');
                    }
					if($('#passwordadmin').val()==""){
                        $('#passwordadmin').addClass('error');
						salah=true;
                    }
                    else{
                        $('#passwordadmin').removeClass('error');
                    }
					if (salah==true)
					{
					$('#salahloginadmin').html("<p class='cap_status_error'>Id Atau Password Anda Masih Kosong</p>").show(3000);
					$('#salahloginadmin').hide(1000);
							return false;	
					}
					$.post( 'index.php/main/ceklogin',{kode : $("#idadmin").val(),password : $("#passwordadmin").val(),authen: "admin" }, function( data){
					var result  = unserialize( data );
					if( result["hasil"]=="Gagal"){
						$('#salahloginadmin').html("<p class='cap_status_error'>Id Atau Password Anda Salah</p>").show(3000);
						$('#salahloginadmin').hide(1000);
						} 
						else {
						$('#passwordadmin').val(result["nama"]);
						$('#formloginadmin').attr( 'action',"");
						return $('#formloginadmin').submit();
							}							
					});					
					return false;
                });	
			//Untuk Login Guru	
			$('#submitloginguru').click(function(e){
				salah=false;
				$('#salahloginguru').hide();
                    if($('#nip').val()==''){
                        $('#nip').addClass('error');
						salah=true;
                    }
                    else{
                        $('#nip').removeClass('error');
                    }
					if($('#passwordguru').val()==""){
                        $('#passwordguru').addClass('error');
						salah=true;
                    }
                    else{
                        $('#passwordguru').removeClass('error');
                    }
					if (salah==true)
					{
					$('#salahloginguru').html("<p class='cap_status_error'>NIP Atau Password Anda Masih Kosong</p>").show(3000);
						$('#salahloginguru').hide(1000);
							return false;	
					}
					$.post( 'index.php/main/ceklogin',{kode : $("#nip").val(),password : $("#passwordguru").val(),authen: "tabel_guru" }, function( data){
					var result  = unserialize( data );
					if( result["hasil"]=="Gagal"){
						$('#salahloginguru').html("<p class='cap_status_error'>NIP Atau Password Anda Salah</p>").show(3000);
						$('#salahloginguru').hide(1000);
						} 
						else {
						 $('#passwordguru').val(result["nama"]);
						 $('#formloginguru').attr( 'action',"");
						return $('#formloginguru').submit();
							}							
					});					
					return false;
                });
				//Untuk Login Siswa
				$('#submitloginsiswa').click(function(e){
				salah=false;
				$('#salahloginsiswa').hide();
                    if($('#nis').val()==''){
                        $('#nis').addClass('error');
						salah=true;
                    }
                    else{
                        $('#nis').removeClass('error');
                    }
					if($('#passwordsiswa').val()==""){
                        $('#passwordsiswa').addClass('error');
						salah=true;
                    }
                    else{
                        $('#passwordsiswa').removeClass('error');
                    }
					if (salah==true)
					{
					$('#salahloginsiswa').html("<p class='cap_status_error'>NIS Atau Password Anda Masih Kosong</p>");
					$('#salahloginsiswa').show(3000);
					$('#salahloginsiswa').hide(1000);
				    return false;	
					}
					$.post( 'index.php/main/ceklogin',{kode : $("#nis").val(),password : $('#passwordsiswa').val(),authen: "tabel_siswa" }, function( data){
					var result  = unserialize( data );
					if( result["hasil"]=="Gagal"){
						$('#salahloginsiswa').html("<p class='cap_status_error'>NIS Atau Password Anda Salah</p>").show(3000);
						$('#salahloginsiswa').hide(1000);
						} 
						else {
						 $('#passwordsiswa').val(result["nama"]);
						  $('#formloginsiswa').attr( 'action',"");
						return $('#formloginsiswa').submit();
							}							
					});	
					return false;
                });
            });
        </script>